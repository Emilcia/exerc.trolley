package PRODUCT;

public class TrolleyPosition {

	private Product product;
	private int amount;
	public int totalPrice;
	
	public Product getProduct() 
	{
		return product;
	}
	public int getAmount() 
	{
		return amount;
	}
	public void setProduct(Product product)
	{
		this.product = product;
	}
	public void setAmount(int amount) 
	{
		this.amount = amount;
	}

	public int totalPrice()
	{
		totalPrice = getAmount()*product.getPrice();
		return totalPrice;
	}
}
