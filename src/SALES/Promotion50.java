package SALES;

import java.util.ArrayList;
import java.util.List;

import PRODUCT.TrolleyPosition;

public class Promotion50 implements Discount {

	List<TrolleyPosition> list = new ArrayList<TrolleyPosition>();
	@Override
	public void setTrolleyPosition(TrolleyPosition a)
	{
		list.add(a);
	}
	@Override
	public void discount() 
	{
		int sum=0;
		for(TrolleyPosition x : list)
		{
			sum=sum+x.totalPrice();
			
			if(sum>50)
			{
				System.out.println("Otrzymujesz kubek gratis");
			}
		}
	}
	

}
